# Step 1: print a greeting
FROM registry.gitlab.com/akosma/greeter:latest
RUN /usr/local/bin/greeter Dockerfile --character snoopy --action think

# Step 2: build runtime image
FROM python:3.7-alpine
RUN apk add fortune
WORKDIR /usr/src/app
COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt
COPY app.py /usr/src/app
COPY templates /usr/src/app/templates/
USER 1001
EXPOSE 9090

CMD [ "python", "app.py" ]

