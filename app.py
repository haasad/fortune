"""
Fortune Cookie Service
"""

import os
from flask import Flask, request, Response, jsonify
from flask.templating import render_template
from subprocess import run, PIPE
from random import randrange

app = Flask(__name__)  # Standard Flask app

version = '1.0'

def get_fortune():
    number = randrange(1000)
    fortune = run('fortune', stdout=PIPE, text=True).stdout
    return number, fortune

@app.route("/")
def fortune():
    """
    Print a random, hopefully interesting, adage
    """
    number, fortune = get_fortune()
    mimetype = request.mimetype
    if mimetype == 'application/json':
        resp = jsonify({ 'number': number, 'fortune': fortune, 'version': version})
        resp.headers['X-Fortune-Version'] = version
        return resp

    if mimetype == 'text/plain':
        result = 'Fortune %s cookie of the day #%s:\n\n%s' % (version, str(number), fortune)
        resp = Response(result, mimetype='text/plain')
        resp.headers['X-Fortune-Version'] = version
        return resp

    # In all other cases, respond with HTML
    html = render_template('fortune.html', number=number, fortune=fortune, version=version)
    resp = Response(html, mimetype='text/html')
    resp.headers['X-Fortune-Version'] = version
    return resp

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=os.environ.get('listenport', 9090))

